@extends('layouts.app')

@section('content')
    <div class="row" style="margin-top: 20px;">
        <div class="column medium-12 medium-centered">
            <h4>Since FA doesn't provide an API, this is a bit more tricky...</h4>
            To make sure the FA account really belongs to you, we will automatically send you a note which will contain a verification code to enter in the next step.<br>
            <b>Please enter your FA username below.</b>
        <form method="POST" action="{{ action('Auth\LoginController@postLoginFA') }}">
            @csrf
            <input type="text" name="FAuser" placeholder="FA username">
            <input type="submit" class="button">
        </form>
    </div>
    </div>
@endsection
