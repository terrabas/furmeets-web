@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="row">
            <div class="column medium-12">
                <h3 style="margin-top: 40px">Log in</h3>
                @if($errors->any())
                    <div class="callout alert small">
                    <h6>{{$errors->first()}}</h6>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="column medium-9">
                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                    @csrf
                    <div>
                        <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>

                    <div>
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div>
                        <div class="col-md-6 offset-md-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>
                    </div>

                    <div>
                            <button type="submit" class="button">
                                {{ __('Login') }}
                            </button>

                            <a class="button warning" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>

                    </div>

                </form>

            </div>
            <div class="column medium-3">
                <div class="row">
                    <h5 style="margin-bottom: 0">Or using</h5>
                </div>
                <div class="row">
                <a href="{!! action('Auth\LoginController@loginSF'); !!}" class="button">SoFurry</a>
                </div>
                <div class="row">
                <a href="{!! action('Auth\LoginController@loginFA'); !!}" class="button">FurAffinity</a>
                </div>
            </div>
        </div>
    </div>
@endsection
