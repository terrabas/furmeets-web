@extends('layouts.app')

@section('content')
    <div class="row" style="margin-top: 20px;">
        <div class="column medium-6 medium-centered">
            <h3>Welcome to FurMeets, {{$username}}!</h3>
            To finish your registration, please enter the e-mail you'd like to send us meet updates to:
            <form method="POST" action="{{ action('Auth\LoginController@postLoginFA') }}">
                @csrf
                <input type="email" name="email" placeholder="E-Mail">
                <input type="submit" class="button">
            </form>
        </div>
    </div>
@endsection
