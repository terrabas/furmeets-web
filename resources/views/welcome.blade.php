@extends('layouts.app')

@section('content')
    <div class="row" style="margin-top: 30px">
        <div class="column medium-12 medium-centered" style="text-align: center">
            <h1>Welcome to FurMeets!</h1>
            <h4>Your place for furry meet information and organisation</h4>
        </div>
    </div>
@endsection
