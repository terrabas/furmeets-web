<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>FurMeets</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">

        <div class="title-bar" data-responsive-toggle="example-menu" data-hide-for="medium">
            <button class="menu-icon" type="button" data-toggle="example-menu"></button>
            <div class="title-bar-title">Menu</div>
        </div>

        <div class="top-bar" id="example-menu">
            <div class="top-bar-left">
                <ul class="dropdown menu" data-dropdown-menu>
                    <li style="font-weight: bold"><a href="/">FurMeets</a></li>
                    <li><a style="padding:0;" href="{!! action('MeetsController@newMeet') !!}"><button type="button" class="button uppercase" style="font-weight: bold">Host meet</button></a></li>
                    <li><a href="{!! action('MeetsController@index') !!}">Browse meets</a></li>
                    <li><input type="search" style="margin-left: 20%" placeholder="Search for meets"></li>
                    <!--<li><button type="button" class="button">Search</button></li> -->
                </ul>
            </div>
            <div class="top-bar-right">
                <ul class="menu">

                    <li><a href="{{url('/login')}}" class="">Log in</a></li>
                    <li><a href="{{url('/register')}}" class="">Register</a></li>

                </ul>
            </div>
        </div>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    @include('layouts.footer')
    <script> $(document).foundation();</script>
</body>
</html>
