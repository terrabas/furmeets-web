<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/login/fa', 'Auth\LoginController@loginFA');
Route::get('/login/sf', 'Auth\LoginController@loginSF');
Route::get('/login/fa/verify', 'Auth\LoginController@verifyLoginFA');
Route::post('/login/fa', 'Auth\LoginController@postLoginFA');
Route::get('/meets', 'MeetsController@index');

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/meets/create', 'MeetsController@newMeet');

});


