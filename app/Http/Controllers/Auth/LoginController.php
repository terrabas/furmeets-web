<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\FA;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginFA()
    {

        return view('auth.falogin');
    }

    public function loginSF(Request $req)
    {
        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId' => '54e5564f8db3da61104fd3569ae735fd',    // The client ID assigned to you by the provider
            'clientSecret' => '4426274427d0444c2c596a484e62faa8',   // The client password assigned to you by the provider
            'redirectUri' => action('Auth\LoginController@loginSF'),
            'urlAuthorize' => 'https://www.sofurry.com/auth/authorize',
            'urlAccessToken' => 'https://www.sofurry.com/auth/token',
            'urlResourceOwnerDetails' => 'none'
        ]);

        // If we don't have an authorization code then get one
        if (!$req->exists('code')) {

            // Fetch the authorization URL from the provider; this returns the
            // urlAuthorize option and generates and applies any necessary parameters
            // (e.g. state).
            $authorizationUrl = $provider->getAuthorizationUrl();

            // Get the state generated for you and store it to the session.
            $_SESSION['oauth2state'] = $provider->getState();

            // Redirect the user to the authorization URL.
            header('Location: ' . $authorizationUrl);
            exit;

            // Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($_GET['state']) || (isset($_SESSION['oauth2state']) && $_GET['state'] !== $_SESSION['oauth2state'])) {

            if (isset($_SESSION['oauth2state'])) {
                unset($_SESSION['oauth2state']);
            }

            exit('Invalid state');

        } else {

            try {

                // Try to get an access token using the authorization code grant.
                $accessToken = $provider->getAccessToken('authorization_code', [
                    'code' => $_GET['code']
                ]);

                $client = new Client();
                $request = $client->get('https://api2.sofurry.com/std/getUserProfile?access_token='.$accessToken->getToken());

                $username = json_decode($request->getBody()->getContents())->username;
                return view('auth.postthirdpartylogin', [
                    'username' => $username
                ]);


            } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

                // Failed to get the access token or user details.
                return redirect('/login')->withErrors(['SoFurry login failed! Please try again.']);

            }

        }
    }

    public function verifyLoginFA()
    {

        return view('auth.falogin_verify');
    }

    public function postLoginFA(Request $req)
    {

        FA::sendNote($req->get('FAuser'), 'FurMeets.info system message', 'This is a test message sent by FurMeets.info');

        return redirect('/login/fa/verify');
    }
}
