<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 10.09.2018
 * Time: 23:11
 */

namespace App\Helpers;


use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use PHPHtmlParser\Dom;

class FA
{
    static function sendNote($userName, $subject, $message){
        $cookieJar = CookieJar::fromArray([
            'a' => env('FA_Cookie_A'),
            'b' => env('FA_Cookie_B'),
        ],'.furaffinity.net');
        $client = new Client();
        $res = $client->request('GET','http://www.furaffinity.net/msg/compose/', ['cookies' => $cookieJar]);

        $dom = new Dom();
        $dom->load($res->getBody()->getContents());

        $inputs = $dom->getElementsByTag("input");

        foreach ($inputs as $input) {
            if ($input->getAttribute("name") == "key") {
                $msgKey = $input->getAttribute("value");
            }
        }
        if($msgKey == null)
            return false;

        $msg = $client->request('POST','http://www.furaffinity.net/msg/send/', ['cookies' => $cookieJar, 'form_params' => [
            'key' => $msgKey,
            'message' => $message,
            'subject' => $subject,
            'to' => $userName,
        ]]);
        if($msg->getStatusCode() == 200)
            return true;
        return false;
    }
}